## DisTok CutTheCord: No Emoji Button

Self explanatory.

![](https://elixi.re/i/u6r3g9qg.png)

->

![](https://elixi.re/i/bqqziruq.png)

#### Available and tested on:
- 10.4.1
- 10.5.1
- 11.0
- 11.4
- 11.5
- 12.0-alpha1
- 11.7
- 12.1
- 12.2
- 12.3
- 13-alpha1
- 12.5
- 14-alpha2
- 14-alpha3
- 14.0
- 15-alpha3
- 15.0
- 16
- 17.0
- 18.0-alpha1
- 18.0-alpha9
- 18.0-alpha11
- 18.0-alpha12
- 19.0
- 20-alpha1
- 21-alpha2
- 22-alpha1
- 21.3
- 22-alpha3-alpha1
- 16
- 17.0
- 18.0-alpha1
- 18.0-alpha9
- 18.0-alpha11
- 18.0-alpha12
- 19.0
- 20-alpha1
- 21-alpha2
- 22-alpha1
- 21.3
- 22-alpha3-alpha4
- 16
- 17.0
- 18.0-alpha1
- 18.0-alpha9
- 18.0-alpha11
- 18.0-alpha12
- 19.0
- 20-alpha1
- 21-alpha2
- 22-alpha1
- 21.3
- 22-alpha3-alpha6
- 16
- 17.0
- 18.0-alpha1
- 18.0-alpha9
- 18.0-alpha11
- 18.0-alpha12
- 19.0
- 20-alpha1
- 21-alpha2
- 22-alpha1
- 21.3
- 22-alpha3-alpha7
- 16
- 17.0
- 18.0-alpha1
- 18.0-alpha9
- 18.0-alpha11
- 18.0-alpha12
- 19.0
- 20-alpha1
- 21-alpha2
- 22-alpha1
- 21.3
- 22-alpha3
- 22.4
- 22.5
- 23.0
- 24-alpha2
- 24
- 28-alpha2
- 28.1
- 29-alpha1
- 30.0
- 30.1
- 31-alpha1
- 31-alpha2
- 32-alpha2
- 32.0
- 33.1
- 34.0
- 34.2
- 34.3
- 35.0-alpha1
- 36.3
- 36.5
- 38.0
- 38.1
- 40.04
- 41.02
- 41.05
- 41.06
- 41.10
- 41.11
- 42.0
- 42.1
- 42.3
- 44-alpha2
- 44-alpha4
- 44.5
- 44.6
- 45.2
- 46.0
- 46.3
- 48.0
- 48.1
- 48.2
- 49.1
- 49.2
- 49.8
- 49.10
- 49.12
- 49.13

